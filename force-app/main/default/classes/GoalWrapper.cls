public class GoalWrapper {
    public String name {get;set;}
    public String primaryCategory {get;set;}
    public String description {get;set;}
    Public Boolean active {get;set;}
    Public Boolean complete {get;set;}
    Public String personResponsible {get;set;}
    public String startDate {get;set;}
	public String endDate {get;set;}
    Public String enablerPP {get;set;}
    Public String enablerOP {get;set;}
    Public String enablerSS {get;set;}
    Public String impactPP {get;set;}
    Public String impactOP {get;set;}
    Public String impactSS {get;set;}
    Public String barrierPP {get;set;}
    Public String barrierOP {get;set;}
    Public String barrierSS {get;set;}
    
    public GoalWrapper(goal__c goal){
        this.name = goal.Name;
        this.primaryCategory= goal.Primary_Category__c;
        this.description = goal.Description__c;
        this.active = goal.Active__c;
        this.complete = goal.Complete__c;
        this.personResponsible = goal.Person_Responsible__c;
        this.startDate = goal.Start_Date__c !=null ? getFormattedDate(goal.Start_Date__c) : null;
        this.endDate = goal.End_Date__c !=null ? getFormattedDate( goal.End_Date__c) : null;
        this.enablerPP = goal.Enablers_Persons_Perspective__c;
        this.enablerOP = goal.Enablers_Others_Perspective__c;
        this.enablerSS = goal.Enablers_Service_or_Support_Provider__c;
        this.impactPP  = goal.Impact_Persons_Perspective__c;
        this.impactOP  = goal.Impact_Others_Perspective__c;
        this.impactSS  = goal.Impact_Service_or_Support_Provider__c;
        this.barrierPP = goal.Barriers_Persons_Perspective__c;
        this.barrierOP = goal.Barriers_Others_Perspective__c;
        this.barrierSS = goal.Barriers_Service_or_Support_Provider__c;
   
    }
    
     public static String getFormattedDate(Date dt){
        String strDate = String.valueOf(dt.day()) + '/' + String.valueOf(dt.month()) + '/' + String.valueOf(dt.year());
        return strDate;
    }
}