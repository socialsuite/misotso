global class ChartAsImage {

    
    @RemoteAction
     global static void  saveChartAsfile(string recordId, String GoalSatisfactionChartData){
        
        ContentVersion GoalSatisfactionChart = new ContentVersion();
        GoalSatisfactionChart.ContentLocation = 'S';
        GoalSatisfactionChart.PathOnClient = 'Chart.png';
        GoalSatisfactionChart.Title = 'Chart-' + 'a4i0T0000005NiHQAU';
        GoalSatisfactionChart.VersionData = EncodingUtil.base64Decode(GoalSatisfactionChartData);
		insert GoalSatisfactionChart;
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = 'a4i0T0000005NiHQAU'; 
        cdl.ContentDocumentId = GoalSatisfactionChart.ContentDocumentId;
        cdl.shareType = 'V';
        insert cdl;
        
            } 

}