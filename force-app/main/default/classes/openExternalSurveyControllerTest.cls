@isTest
public class openExternalSurveyControllerTest {
    @isTest
    public static void testConstructor() {
        String siteName = 'survey';
        Site[] orgSites = [SELECT Id, UrlPathPrefix FROM Site WHERE Name =: siteName];
        openExternalSurveyController controller = new openExternalSurveyController();
        if (orgSites.size() > 0) {
            System.assert(controller.surveyUrl != null);
        } else {
            System.assertEquals(null, controller.surveyUrl);
        }
    }
}