@isTest
public class SocialsuiteUtilitiesTest {
	@isTest
    public static void testOrphanRecordsDeletion() {
        Goal__c goal = new Goal__c(Name='Test');
        insert goal;
        
        SocialsuiteUtilities.deleteOrphanRecords();
        
        List<Goal__c> goals = [SELECT Id FROM Goal__c];
        System.assertEquals(0, goals.size());
    }
}