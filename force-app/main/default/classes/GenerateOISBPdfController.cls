public class GenerateOISBPdfController {
/***************************************************************************************************************************************************************
*********  Objective : Generate PDF Version for O&ISB Report
*********  Version   : 1.0 
*********  Date      : 1-December-2020
*********  Author    : TquilaANZ
****************************************************************************************************************************************************************/
    public  socialsuite__Beneficiary__c beneficiaryrec {set;get;}
    public String recId {set;get;}
    Public String chartId{set;get;}
    Public String legendId{set;get;}
    
/***************************************************************************************************************************************************************
*********  Custom controller for beneficiary record
****************************************************************************************************************************************************************/

    public GenerateOISBPdfController (ApexPages.StandardController controller){
          socialsuite__Beneficiary__c record = (socialsuite__Beneficiary__c)controller.getRecord();
          Id recId= record.Id;
          ChartId = ApexPages.currentPage().getParameters().get('chartId');
          legendId = ApexPages.currentPage().getParameters().get('legendId');  
    }
    
    
}