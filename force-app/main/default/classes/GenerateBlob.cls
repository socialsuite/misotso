public class GenerateBlob {
    public  id Id{get;set;}
    
    Public GenerateBlob(Apexpages.StandardController controller){
      socialsuite__Beneficiary__c record = (socialsuite__Beneficiary__c)controller.getRecord(); 
      this.Id= record.id;
    }
    public  void generatePDF(){
			
          Pagereference page = APEXPAGES.currentPage();
      	  Blob goalreport = page.getContentAsPDF();
          Datetime now = Datetime.now();
          Integer offset = UserInfo.getTimezone().getOffset(now);
          Datetime local = now.addSeconds(offset/1000);
          ContentVersion conVer = new ContentVersion();
          conVer.ContentLocation = 'S'; 
          conVer.PathOnClient = 'goalreport.pdf'; 
          conVer.Title = 'Goal Report '+local; 
          conVer.VersionData =goalreport;  
          insert conVer;
 
          // First get the content document Id from ContentVersion
          Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
           
          
          
          //Create ContentDocumentLink
           ContentDocumentLink cDe = new ContentDocumentLink();
           cDe.ContentDocumentId = conDoc;
           cDe.LinkedEntityId = Id;
           cDe.ShareType = 'I'; // Inferred permission
           cDe.Visibility = 'InternalUsers';
           insert cDe;
    
        
    }
 
       
    }