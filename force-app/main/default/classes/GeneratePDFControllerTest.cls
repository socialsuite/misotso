@isTest
public class GeneratePDFControllerTest {
/***************************************************************************************************************************************************************
*********  Objective : Test Class for Goals Report and O&IS B Report
*********  Version   : 1.0 
*********  Date      : 1-December-2020
*********  Author    : TquilaANZ
*****************************************************************************************************************************************************************/
   
    public static socialsuite__Beneficiary__c beneficiaryrec {set;get;}
    public static socialsuite__Location__c cohort;
    public static date startdate;
    public static date  enddate;
    
    @testSetup static void createbeneficiaryrec() {
       
        startdate=date.today();
        enddate =date.today().addYears(1);
        socialsuite__Program__c program = new socialsuite__Program__c ( Name= 'test Program',socialsuite__Start_Date__c = startdate,
                                                                       socialsuite__End_Date__c=enddate );
        insert program;
        socialsuite__Activity__c activity  = new  socialsuite__Activity__c (Name='test Activity',socialsuite__Program__c= program.id,
                                                                           	socialsuite__Status__c='Active');
        insert activity;
        
        socialsuite__Data_Collection_Plan__c datacoll = new socialsuite__Data_Collection_Plan__c(Name='test Data collection',
                                                                socialsuite__Activity__c=activity.id,socialsuite__Status__c='Active');
        insert datacoll;
        cohort = new socialsuite__Location__c(name= 'test cohort',	socialsuite__Status__c='Active',
                                             socialsuite__Data_Collection_Plan__c= datacoll.id,
                                             socialsuite__Activity__c= Activity.id,
                                             socialsuite__End_Date__c = enddate,socialsuite__Start_Date__c= startdate);
        insert cohort;
        beneficiaryrec = new socialsuite__Beneficiary__c(Name='Test Beneficiary',socialsuite__Location__c= cohort.id,	
                                                         socialsuite__Status__c='Active',
                                             socialsuite__Data_Collection_Start_Date__c= startdate);
        insert beneficiaryrec;
        Goal__c goal = new Goal__c(name = 'Test Goal', 	Beneficiary__c = beneficiaryrec.id);
        insert goal;
    }
        
    @istest
    public static void generateGoalreports(){
        	
        	socialsuite__Beneficiary__c beneficiaryrecord = [select id from socialsuite__Beneficiary__c where name = 'Test Beneficiary' limit 1];
			ApexPages.StandardController stdController= new ApexPages.StandardController(beneficiaryrecord);
			GeneratePdfController  testBeneficiaryGoals = new GeneratePdfController(stdController);
        	String satisfaction ='iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
		    String performance  = 'iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
        	string[] charts= GeneratePDFBlob.saveSatisfactionChartAsfile(beneficiaryrecord.id,satisfaction,performance);
        	PageReference pdfPage = Page.Goal_reports_pdf;
       		GeneratePDFBlob newblob= new GeneratePDFBlob(stdController);
        	newblob.chart1id = charts[0];
            newblob.chart2id = charts[1];
            newblob.Id = beneficiaryrecord.id;
        	newblob.pageTransfer();

    }
    @istest
     public static void generateOISBreports(){
            system.debug('inside generateOISBreports');
        	socialsuite__Beneficiary__c beneficiaryrecord2 = [select id from socialsuite__Beneficiary__c where name = 'Test Beneficiary' limit 1];
         	ApexPages.StandardController stdController= new ApexPages.StandardController(beneficiaryrecord2);
         	GenerateOISBPdfController  testBeneficiaryOISB = new GenerateOISBPdfController(stdController);
			String chartData ='iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
		    String legendData  = 'iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
			string[] charts=GenerateOISBPDFBlob.saveOISBChartAsfile(beneficiaryrecord2.id, chartData, legendData );
            PageReference pdfPage = Page.OISB_pdf;
            pdfPage.getParameters().put('chartId',charts[0]);
           pdfPage.getParameters().put('legendId',charts[1]);
         	
       		GenerateOISBPDFBlob newblob1= new GenerateOISBPDFBlob(stdController);
        	newblob1.chartId = charts[0];
            newblob1.legendId = charts[1];
        	newblob1.pageTransfer();
        
    }

}