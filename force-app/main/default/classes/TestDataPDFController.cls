@isTest
public class TestDataPDFController {
    public static socialsuite__Beneficiary__c beneficiaryrec {set;get;}
    public static socialsuite__Location__c cohort;
    public static date startdate;
    public static date  enddate;
    
    @testSetup public static void createbeneficiaryrec() {
       
        startdate=date.today();
        enddate =date.today().addYears(1);
        socialsuite__Program__c program = new socialsuite__Program__c ( Name= 'test Program',socialsuite__Start_Date__c = startdate,
                                                                       socialsuite__End_Date__c=enddate );
        insert program;
        socialsuite__Activity__c activity  = new  socialsuite__Activity__c (Name='test Activity',socialsuite__Program__c= program.id,
                                                                            socialsuite__Status__c='Active');
        insert activity;
        
        socialsuite__Data_Collection_Plan__c datacoll = new socialsuite__Data_Collection_Plan__c(Name='test Data collection',
                                                                socialsuite__Activity__c=activity.id,socialsuite__Status__c='Active');
        insert datacoll;
        cohort = new socialsuite__Location__c(name= 'test cohort',  socialsuite__Status__c='Active',
                                             socialsuite__Data_Collection_Plan__c= datacoll.id,
                                             socialsuite__Activity__c= Activity.id,
                                             socialsuite__End_Date__c = enddate,socialsuite__Start_Date__c= startdate);
        insert cohort;
        beneficiaryrec = new socialsuite__Beneficiary__c(Name='Test Beneficiary',socialsuite__Location__c= cohort.id,   
                                                         socialsuite__Status__c='Active',
                                             socialsuite__Data_Collection_Start_Date__c= startdate);
        insert beneficiaryrec;
        Goal__c goal = new Goal__c(name = 'Test Goal',  Beneficiary__c = beneficiaryrec.id);
        insert goal;
    }
    

}