public with sharing class openExternalSurveyController {
    public static final String SITE_NAME = 'survey';
    public String surveyUrl { get; set; }
    
    public openExternalSurveyController() {
        Map<String, String> siteDetails = openExternalSurveyController.getSiteDetails();
        if (siteDetails != null) {
            String domainAddress = siteDetails.get('domainAddress');
            String suffix = siteDetails.get('suffix');
            this.surveyUrl = domainAddress += '?';
            Map<String, String> queryParams = apexPages.currentPage().getParameters();
            if (queryParams != null) {
                for (String keyString : queryParams.keySet()) {
                    this.surveyUrl += keyString + '=' + queryParams.get(keyString) + '&';
                }
                this.surveyUrl = this.surveyUrl.removeEnd('&');
            }
        }
    }
    public static Map<String, String> getSiteDetails() {
        try{
            Site[] sites = [SELECT Id, UrlPathPrefix FROM Site WHERE Name =: SITE_NAME];
            SiteDetail mySiteDetail = [SELECT SecureURL FROM SiteDetail WHERE DurableId = :sites[0].Id];
            return new Map<String, String>{
                'domainAddress' => mySiteDetail.SecureURL,
                'suffix' => sites[0].UrlPathPrefix
            };
        } catch(Exception ex) {
            return null;
        }
    }
}