@isTest
public class GeneratePDFControllerTestGoal {
    public static socialsuite__Beneficiary__c beneficiaryrec {set;get;}
    public static socialsuite__Location__c cohort;
    public static date startdate;
    public static date  enddate;
    @istest
    public static void generateGoalreports(){
            TestDataPDFController.createbeneficiaryrec();
            socialsuite__Beneficiary__c beneficiaryrecord = [select id from socialsuite__Beneficiary__c where name = 'Test Beneficiary' limit 1];
            ApexPages.StandardController stdController= new ApexPages.StandardController(beneficiaryrecord);
            GeneratePdfController  testBeneficiaryGoals = new GeneratePdfController(stdController);
            String satisfaction ='iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
            String performance  = 'iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
            string[] charts= GeneratePDFBlob.saveSatisfactionChartAsfile(beneficiaryrecord.id,satisfaction,performance);
            PageReference pdfPage = Page.Goal_reports_pdf;
            GeneratePDFBlob newblob= new GeneratePDFBlob(stdController);
            newblob.chart1id = charts[0];
            newblob.chart2id = charts[1];
            newblob.Id = beneficiaryrecord.id;
            newblob.pageTransfer();

    }

}