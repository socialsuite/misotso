public class Beneficiarywrapper {
    
    Public socialsuite__Beneficiary__c ssbeneficiaryrec {get;set;}
    public list<GoalWrapper> goalwrapperlist{get;set;}

    public Beneficiarywrapper(socialsuite__Beneficiary__c beneficiaryRec){
        ssbeneficiaryrec = beneficiaryRec;
        goalwrapperlist = new list<goalwrapper>();
      
        Try{
            for (goal__c eachgoal:beneficiaryRec.goals__r){
              // goalwrapperlist.add(goalwrapper(eachgoal)) ;
              GoalWrapper goal= new goalwrapper(eachgoal);
              goalwrapperlist.add(goal) ;

            }
           
        } catch(exception e){
            System.debug(E.getMessage()+' '+e.getStackTraceString());
        }
    }
        
        
}