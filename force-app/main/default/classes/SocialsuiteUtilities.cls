public class SocialsuiteUtilities {
	@InvocableMethod(label='Delete Orphan Records' description='Deletes orphan records')
    public static void deleteOrphanRecords() {
        //Socialsuite v2.9
        /*** Activity down ***/
        List<socialsuite__Activity__c> activities = [SELECT Id, Name FROM socialsuite__Activity__c WHERE socialsuite__Program__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan activities number: '+activities.size());
        System.debug(LoggingLevel.WARN, 'Orphan activities: '+activities);
        delete activities;
         
        List<socialsuite__Data_Collection_Plan__c> dcps= [SELECT Id, Name FROM socialsuite__Data_Collection_Plan__c WHERE socialsuite__Activity__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan dcps number: '+dcps.size());
        System.debug(LoggingLevel.WARN, 'Orphan dcps : '+dcps);
        delete dcps;
           
        List<socialsuite__Survey_Point__c> surveyPoints = [SELECT Id, Name FROM socialsuite__Survey_Point__c WHERE socialsuite__Activity__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan survey points number: '+surveyPoints.size());
        System.debug(LoggingLevel.WARN, 'Orphan survey points: '+surveyPoints);
        delete surveyPoints;
          
        List<socialsuite__Map__c> maps = [SELECT Id FROM socialsuite__Map__c WHERE socialsuite__Target_Outcome__c = null OR socialsuite__Survey_Point__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan maps number: '+maps .size());
        System.debug(LoggingLevel.WARN, 'Orphan maps: '+maps );
        delete maps;
           
        List<socialsuite__Location__c> locations = [SELECT Id, Name FROM socialsuite__Location__c WHERE socialsuite__Activity__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan locations number: '+locations.size());
        System.debug(LoggingLevel.WARN, 'Orphan locations: '+locations);
        delete locations;
           
        List<socialsuite__Survey_Template__c> surveyTemplates = [SELECT Id, Name FROM socialsuite__Survey_Template__c WHERE socialsuite__Survey_Point__c = null AND socialsuite__Type__c = 'Survey'];
        System.debug(LoggingLevel.WARN, 'Orphan survey templates number: '+surveyTemplates.size());
        System.debug(LoggingLevel.WARN, 'Orphan survey templates: '+surveyTemplates);
        delete surveyTemplates;
           
        List<socialsuite__Survey_Section__c> surveySections = [SELECT Id, Name FROM socialsuite__Survey_Section__c WHERE socialsuite__Survey_Template__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan survey sections number: '+surveySections.size());
        System.debug(LoggingLevel.WARN, 'Orphan survey sections: '+surveySections);
        delete surveySections;
           
        List<socialsuite__Survey_Component__c> surveyComponents = [SELECT Id, Name FROM socialsuite__Survey_Component__c WHERE socialsuite__Survey_Section__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan survey components number: '+surveyComponents.size());
        System.debug(LoggingLevel.WARN, 'Orphan survey components: '+surveyComponents);
        delete surveyComponents;
         
        List<socialsuite__Survey_Component_to_Question__c> surveyComponentsToQuestions = [SELECT Id, Name FROM socialsuite__Survey_Component_to_Question__c WHERE socialsuite__Survey_Component__c = null OR socialsuite__Question__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan survey components to questions mappings number: '+surveyComponentsToQuestions .size());
        System.debug(LoggingLevel.WARN, 'Orphan survey components to questions mappings : '+surveyComponentsToQuestions );
        delete surveyComponentsToQuestions ;
           
        /*** Beneficiary down ***/
        List<socialsuite__Beneficiary__c> beneficiaries = [SELECT Id, Name, socialsuite__Survey_Point__c FROM socialsuite__Beneficiary__c WHERE socialsuite__Location__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan beneficiaries number: '+beneficiaries.size());
        System.debug(LoggingLevel.WARN, 'Orphan beneficiaries: '+beneficiaries);
        delete beneficiaries;
           
        List<socialsuite__Reviewer__c> reviewers = [SELECT Id, Name, socialsuite__Survey_Point__c FROM socialsuite__Reviewer__c WHERE socialsuite__Beneficiary__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan reviewers number: '+reviewers.size());
        System.debug(LoggingLevel.WARN, 'Orphan reviewers: '+reviewers);
        delete reviewers;
           
        List<socialsuite__Survey__c> surveys = [SELECT Id, Name, socialsuite__Survey_Point__c FROM socialsuite__Survey__c WHERE socialsuite__Reviewer__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan surveys number: '+surveys.size());
        System.debug(LoggingLevel.WARN, 'Orphan surveys: '+surveys);
        delete surveys;
           
        List<socialsuite__Schedulable_Action__c> sas= [SELECT Id, Name FROM socialsuite__Schedulable_Action__c WHERE socialsuite__Survey__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan sas number: '+sas.size());
        System.debug(LoggingLevel.WARN, 'Orphan sas: '+ sas);
        delete sas;
           
        List<socialsuite__Participant__c> part = [SELECT Id, Name FROM socialsuite__Participant__c WHERE socialsuite__Beneficiary__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan part number: '+part.size());
        System.debug(LoggingLevel.WARN, 'Orphan part: '+ part);
        delete part;
           
        List<socialsuite__Response__c> responses = [SELECT Id, Name, socialsuite__Status__c FROM socialsuite__Response__c WHERE socialsuite__Reviewer__c = null AND socialsuite__Survey__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan responses number: '+responses.size());
        System.debug(LoggingLevel.WARN, 'Orphan responses: '+responses);
        delete responses;
        
        List<Goal__c> goals = [SELECT Id FROM Goal__c WHERE Reviewer__c = null];
        System.debug(LoggingLevel.WARN, 'Orphan goals number: '+goals.size());
        System.debug(LoggingLevel.WARN, 'Orphan goals: '+goals);
        delete goals;
    }
}