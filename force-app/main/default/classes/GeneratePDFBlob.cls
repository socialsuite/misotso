public class GeneratePDFBlob {
/***************************************************************************************************************************************************************
*********  Objective : Generate PDF Version for Goals Report
*********  Version   : 1.0 
*********  Date      : 1-December-2020
*********  Author    : TquilaANZ
****************************************************************************************************************************************************************/

    public  id Id{get;set;}
    public  id recordId{get;set;}
    public static list<String> chartsId{get;set;} 
    public static String satisfactionChartId;
    public static String performanceChartId;
    public static String GoalSatisfactionChartCDId;
    public static String GoalPerformanceChartCDId;
    public String chart1id {get;set;}
    public String chart2id {get;set;} 
    
    
    
/***************************************************************************************************************************************************************
*********  Retrieve the id of beneficiary record
****************************************************************************************************************************************************************/
        
    Public GeneratePDFBlob(Apexpages.StandardController controller){
      socialsuite__Beneficiary__c record = (socialsuite__Beneficiary__c)controller.getRecord(); 
      this.Id= record.id;
    }
    
/***************************************************************************************************************************************************************
*********  Save the charts as Image on record
****************************************************************************************************************************************************************/

    @RemoteAction
     public static list<String>  saveSatisfactionChartAsfile(string recordId, String satisfactionImageDataStr, String performanceImageDataStr ){
         try{
            system.debug('pic 1: ' + satisfactionImageDataStr);
            system.debug('pic 2: ' + performanceImageDataStr);
            list<String> chartsId = new List<String>();
            list<ContentDocumentLink> cdlList = new list<ContentDocumentLink>();
            String satisfactionChartId;
            String performanceChartId;
            List<SocialSuite_CD_Id__c> sccdlist = new List<SocialSuite_CD_Id__c>();
             
            ContentVersion GoalSatisfactionChartVersion = new ContentVersion();
            GoalSatisfactionChartVersion.ContentLocation = 'S';
            GoalSatisfactionChartVersion.PathOnClient = 'GoalSatisfactionChart.png';
            GoalSatisfactionChartVersion.Title = 'Chart-' + recordId;
            GoalSatisfactionChartVersion.VersionData = EncodingUtil.base64Decode(satisfactionImageDataStr);
            
            ContentVersion GoalPerformanceChartVersion = new ContentVersion();
            GoalPerformanceChartVersion.ContentLocation = 'S';
            GoalPerformanceChartVersion.PathOnClient = 'GoalPerformanceChart.png';
            GoalPerformanceChartVersion.Title = 'Chart-' + recordId;
            GoalPerformanceChartVersion.VersionData = EncodingUtil.base64Decode(performanceImageDataStr);
             
            insert new List<ContentVersion>{GoalSatisfactionChartVersion, GoalPerformanceChartVersion};
    
            for(ContentVersion cv: [SELECT Id,ContentDocumentId,  PathOnClient FROM ContentVersion 
                                    WHERE Id =:GoalSatisfactionChartVersion.Id OR Id=:GoalPerformanceChartVersion.Id]){
                 
                 if (cv.id == GoalSatisfactionChartVersion.id) GoalSatisfactionChartCDId  = cv.ContentDocumentId;
                 if (cv.id == GoalPerformanceChartVersion.id) GoalPerformanceChartCDId = cv.ContentDocumentId;
                 ContentDocumentLink cdl = New ContentDocumentLink();
                 cdl.LinkedEntityId = recordId; 
                 cdl.ContentDocumentId = cv.ContentDocumentId;
                 cdl.shareType = 'V';  
                 cdlList.add(cdl);                    
               
            }
            insert cdlList;
            
            for (ContentDocumentLink file:[SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId,
                                          ContentDocument.LatestPublishedVersion.Title
                                          FROM ContentDocumentLink WHERE id=:cdlList[0].id or id=:cdlList[1].id]){
                if (file.ContentDocumentId == GoalSatisfactionChartCDId) satisfactionChartId = file.ContentDocument.LatestPublishedVersionId;                                
                if (file.ContentDocumentId == GoalPerformanceChartCDId)performanceChartId = file.ContentDocument.LatestPublishedVersionId;                               
                                                     
               sccdlist.add(new SocialSuite_CD_Id__c(Content_Document_ID__c=file.ContentDocumentId));                             
             }
            
             insert  sccdlist;													 
             
            //LINK FILE TO RECORD
             chartsId.add(satisfactionChartId);
             
             chartsId.add(performanceChartId);
             System.debug('inside fileSaving '+ chartsId);
             
             //call future methid delayed by 5 mins
             return chartsId; 
        	 }catch(exception e){
              throw new AuraHandledException('An unexpected error has occured'+ e.getMessage());
			}
      } 

     
/***************************************************************************************************************************************************************
*********  Transfer to the PDF page
****************************************************************************************************************************************************************/
       
    public pagereference pageTransfer(){
        try{
           Datetime now = Datetime.now();
           Integer offset = UserInfo.getTimezone().getOffset(now);
           Datetime local = now.addSeconds(offset/1000);
           pageReference pg = new pagereference('/apex/goal_reports_pdf'); 
           pg.getParameters().put('chart1id',chart1id);
           pg.getParameters().put('chart2id',chart2id);
           pg.getParameters().put('Id',id);
            blob goalreportblob = !Test.isRunningTest() ? pg.getContent(): Blob.valueOf('Fake content');
           ContentVersion goalreport = new ContentVersion();
           goalreport.ContentLocation = 'S';
           goalreport.PathOnClient = 'GoalReport.pdf';
           goalreport.Title = 'Goal Report -' + local+'.pdf';
           goalreport.VersionData = goalreportblob;
           insert goalreport;
           ContentVersion cv= [SELECT Id,ContentDocumentId,  PathOnClient FROM ContentVersion 
                                    WHERE Id =:goalreport.Id];
           ContentDocumentLink cdl = New ContentDocumentLink();
           cdl.LinkedEntityId = id; 
           cdl.ContentDocumentId = cv.ContentDocumentId;
           cdl.shareType = 'V';  
           insert cdl; 
           return pg;
        }catch(exception e){
              throw new AuraHandledException('An unexpected error has occured'+ e.getMessage());
		}
    }
     
 }