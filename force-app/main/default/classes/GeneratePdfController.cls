public class GeneratePdfController {
    
/***************************************************************************************************************************************************************
*********  Objective : Generate PDF Version for Goals Report
*********  Version   : 1.0 
*********  Date      : 1-December-2020
*********  Author    : TquilaANZ
****************************************************************************************************************************************************************/
    
    
/***************************************************************************************************************************************************************
*********  Custom controller for beneficiary record
****************************************************************************************************************************************************************/

    public  socialsuite__Beneficiary__c beneficiaryrec {set;get;}
    public List<goal__c> goalList {set;get;}
    public Beneficiarywrapper beneficiarywrapper{get;set;}
   /* public String recId {set;get;}
    public String chart1 {set;get;}
    public String chart2 {set;get;}
    Public String oISBChartId{set;get;}
    Public String oISBLegendId{set;get;}*/
 

    
  public GeneratePdfController (ApexPages.StandardController controller){
 
          socialsuite__Beneficiary__c record = (socialsuite__Beneficiary__c)controller.getRecord();
          Id recId= record.Id;
          List<goal__c> goalList = new List<goal__c>();
          beneficiaryrec= [Select id, (select Name, Primary_Category__c,Description__c,  
                                 Active__c, Complete__c,Person_Responsible__c,
                                 Start_Date__c,End_Date__c, Barriers_Others_Perspective__c,
                                 Barriers_Persons_Perspective__c,Barriers_Service_or_Support_Provider__c,
                                 Enablers_Others_Perspective__c, Enablers_Persons_Perspective__c, 
                                 Enablers_Service_or_Support_Provider__c ,Impact_Others_Perspective__c,
                                 Impact_Persons_Perspective__c, Impact_Service_or_Support_Provider__c 
                                 from  Goals__r) 
                                 from socialsuite__Beneficiary__c
                                    where 	id=:recId limit 1];
          
             beneficiarywrapper = new Beneficiarywrapper(beneficiaryrec);
           
     
  }
}