public class GenerateOISBPDFBlob {
/***************************************************************************************************************************************************************
*********  Objective : Generate PDF Version for O&IS B Report
*********  Version   : 1.0 
*********  Date      : 1-December-2020
*********  Author    : TquilaANZ
****************************************************************************************************************************************************************/
    public  id Id{get;set;}
    public static list<String> chartsId{get;set;}
    public static String chartCDId;
    public static String legendCDId;
    public static String OISBChartId;
    public static String OISBChartLegendId;
    public String chartId {get;set;}
    public String legendId {get;set;}  
    
    
/***************************************************************************************************************************************************************
*********  Retrieve the id of beneficiary record
****************************************************************************************************************************************************************/   
      Public GenerateOISBPDFBlob(Apexpages.StandardController controller){
      	socialsuite__Beneficiary__c record = (socialsuite__Beneficiary__c)controller.getRecord(); 
      	this.Id= record.id;
   	 }

/***************************************************************************************************************************************************************
*********  Save the charts as Image on record
****************************************************************************************************************************************************************/
   
       @RemoteAction
      public static list<String>  saveOISBChartAsfile(string recordId, String chartData, String legendData ){
        List<String> chartsId = new List<String>();
        List<ContentVersion> cvList = new List<ContentVersion>();
        List<ContentDocumentLink> cdlList = new list<ContentDocumentLink>();
        List<SocialSuite_CD_Id__c> sccdlist = new List<SocialSuite_CD_Id__c>();
          try{    
                ContentVersion chart = new ContentVersion();
                chart.ContentLocation = 'S';
                chart.PathOnClient = 'OISBChart.png';
                chart.Title = 'OISBChart-' + recordId;
                chart.VersionData = EncodingUtil.base64Decode(chartData);
                
                ContentVersion legend = new ContentVersion();
                legend.ContentLocation = 'S';
                legend.PathOnClient = 'OISBChartLegend.png';
                legend.Title = 'OISBChartLegend-' + recordId;
                legend.VersionData = EncodingUtil.base64Decode(legendData);
                 
                cvList.add(chart);
                cvList.add(legend);
                insert cvlist;
                  
                
                
                for(ContentVersion cv: [SELECT Id,ContentDocumentId,  PathOnClient FROM ContentVersion 
                                        WHERE Id =:chart.Id OR Id=:legend.Id]){ 
                        if (cv.id == chart.id) chartCDId  = cv.ContentDocumentId;
                        if (cv.id == legend.id) legendCDId = cv.ContentDocumentId;
                            
                        ContentDocumentLink cdl = New ContentDocumentLink();
                        cdl.LinkedEntityId = recordId; 
                        cdl.ContentDocumentId = cv.ContentDocumentId;
                        cdl.shareType = 'V';
                       cdlList.add(cdl);                     
                    }                           
                
                 
                insert cdlList;
            
                for (ContentDocumentLink file:[SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId,
                                              ContentDocument.LatestPublishedVersion.Title
                                              FROM ContentDocumentLink WHERE id=:cdlList[0].id or id=:cdlList[1].id]){
                    if (file.ContentDocumentID == chartCDId){
                        OISBChartId = file.ContentDocument.LatestPublishedVersionId;                               
                      }
                    if (file.ContentDocumentID == legendCDId){
                        OISBChartLegendId = file.ContentDocument.LatestPublishedVersionId;                               
                      }                               
                   sccdlist.add(new SocialSuite_CD_Id__c(Content_Document_ID__c=file.ContentDocumentId));                                           
                 }
                insert sccdlist;
                 
                //LINK FILE TO RECORD
                 chartsId.add(OISBChartId);
                 chartsId.add(OISBChartLegendId);
                 return chartsId; 
          }catch(exception e){
              throw new AuraHandledException('An unexpected error has occured'+ e.getMessage());
          }
      } 

    

/***************************************************************************************************************************************************************
*********  Transfer to the PDF page
****************************************************************************************************************************************************************/

      public pagereference pageTransfer(){
       try{
           Datetime now = Datetime.now();
           Integer offset = UserInfo.getTimezone().getOffset(now);
           Datetime local = now.addSeconds(offset/1000);
           pageReference pg = new pagereference('/apex/OISB_pdf'); 
           pg.getParameters().put('chartId',chartId);
           pg.getParameters().put('legendId',legendId);
           blob OISBblob = !Test.isRunningTest() ? pg.getContent(): Blob.valueOf('Fake content');
           ContentVersion OISBReport = new ContentVersion();
           OISBReport.ContentLocation = 'S';
           OISBReport.PathOnClient = 'O&IS Report.pdf';
           OISBReport.Title = 'O&IS-B Report-' + local+'.pdf';
           OISBReport.VersionData = OISBblob;
           insert OISBReport;
           ContentVersion cv= [SELECT Id,ContentDocumentId,  PathOnClient FROM ContentVersion 
                                    WHERE Id =:OISBReport.Id];
           ContentDocumentLink cdl = New ContentDocumentLink();
           cdl.LinkedEntityId = id; 
           cdl.ContentDocumentId = cv.ContentDocumentId;
           cdl.shareType = 'V';  
           insert cdl; 
           return pg;
       } catch(exception e){
              throw new AuraHandledException('An unexpected error has occured'+ e.getMessage());
		}
 	 } 
     

}