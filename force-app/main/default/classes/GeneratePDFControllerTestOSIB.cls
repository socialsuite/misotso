@isTest
public class GeneratePDFControllerTestOSIB {
    public static socialsuite__Beneficiary__c beneficiaryrec {set;get;}
    public static socialsuite__Location__c cohort;
    public static date startdate;
    public static date  enddate; 
    
    
    @istest
     public static void generateOISBreports(){
            system.debug('inside generateOISBreports');
            TestDataPDFController.createbeneficiaryrec();
            socialsuite__Beneficiary__c beneficiaryrecord2 = [select id from socialsuite__Beneficiary__c where name = 'Test Beneficiary' limit 1];
            ApexPages.StandardController stdController= new ApexPages.StandardController(beneficiaryrecord2);
            GenerateOISBPdfController  testBeneficiaryOISB = new GenerateOISBPdfController(stdController);
            String chartData ='iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
            String legendData  = 'iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAIAAAA7ljmRAAAAGElEQVQIW2P4DwcMDAxAfBvMAhEQMYgcACEHG8ELxtbPAAAAAElFTkSuQmCC';
            string[] charts=GenerateOISBPDFBlob.saveOISBChartAsfile(beneficiaryrecord2.id, chartData, legendData );
            PageReference pdfPage = Page.OISB_pdf;
            pdfPage.getParameters().put('chartId',charts[0]);
           pdfPage.getParameters().put('legendId',charts[1]);
            
            GenerateOISBPDFBlob newblob1= new GenerateOISBPDFBlob(stdController);
            newblob1.chartId = charts[0];
            newblob1.legendId = charts[1];
            newblob1.pageTransfer();
        
    }
    

}